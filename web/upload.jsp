<%-- 
    Document   : upload
    Created on : 04-abr-2017, 14:31:27
    Author     : mfontana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Exemple to Upload a File</title>
    </head>
    <body>
         <form method="post" action="uploadFile" enctype="multipart/form-data">
            Select file to upload:
            <input type="file" name="file" />
            <input type="submit" value="Upload" />
        </form>
    </body>
</html>
