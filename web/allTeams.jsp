<%-- 
    Document   : allTeams
    Created on : 21-mar-2017, 16:58:29
    Author     : mfontana
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@page import="entities.Team"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Teams</title>
    </head>
    <body>
        <h1>Teams</h1>
        <table>
            <tr>
                <th>Name</th>
                <th>City</th>
                <th>Creation</th>
            </tr>
        <% 
        List<Team> teams = (List<Team>) request.getAttribute("teams");
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        for (Team team : teams) {
            %>
            <tr>
                <td><%= team.getName() %></td> 
                <td><%= team.getCity()%></td> 
                <td><%= sdf.format(team.getCreation())%></td> 
            </tr>
        <% }
        %>
        </table>
    </body>
</html>
