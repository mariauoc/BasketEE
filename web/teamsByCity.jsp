<%-- 
    Document   : teamsByCity
    Created on : 20-mar-2018, 12:58:15
    Author     : mfontana
--%>

<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Teams By City</title>
    </head>
    <body>
        <%
            List<String> cities = (List<String>) request.getAttribute("cities");
            if (cities != null) {
                if (cities.isEmpty()) {
        %>
        <p>There are no teams.</p>
        <%
        } else {
        %>
        <form action="TeamsByCity" method="POST">
            <p>Select a city: 
                <select name="city">
                    <%
                        for (String c : cities) {
                    %>
                    <option> <%= c%> </option>
                    <%
                        }
                    %>
                </select>
            </p>
            <p><input type="submit" value="View Teams" name="citySelected"></p>
        </form>
        <%
                }
            }
        %>
    </body>
</html>
