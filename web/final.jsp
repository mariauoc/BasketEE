<%-- 
    Document   : final
    Created on : 21-mar-2017, 16:14:04
    Author     : mfontana
--%>

<%@page import="servlets.UploadFile"%>
<%@page import="servlets.NewTeam"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Done</title>
    </head>
    <body>
        <%
            String status = (String) request.getAttribute("status");
            if (status.equals(NewTeam.STATUS_OK)) { %>
        <p>Team registered.</p> 
        <% } else if (status.equals(NewTeam.STATUS_ERROR)) { %>
        <p>Team already exists.</p>
        <% } else if(status.equals(UploadFile.STATUS_OK)) {%>
        <p>Upload has been done successfully!.</p>
        <% } %>
        <a href="index.jsp">Main menu</a>
    </body>
</html>
