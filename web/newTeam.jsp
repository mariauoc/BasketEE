<%-- 
    Document   : newTeam
    Created on : 21-mar-2017, 15:40:54
    Author     : mfontana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>New Team</title>
    </head>
    <body>
        <h1>New Team Form</h1>
        <form action="NewTeam" method="POST">
            <p>Name: <input type="text" name="name"></p>
            <p>City: <input type="text" name="city"></p>
            <p>Creation date: <input type="date" name="creation"></p>
            <p><input type="submit" value="New" name="newteam"></p>
        </form>
    </body>
</html>
