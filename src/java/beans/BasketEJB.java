/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import entities.Team;
import exceptions.BasketException;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

/**
 *
 * @author mfontana
 */
@Stateless
public class BasketEJB {

    @PersistenceUnit
    EntityManagerFactory emf;

    public void insertTeam(Team t) throws BasketException {
        EntityManager em = emf.createEntityManager();
        Team exist = em.find(Team.class, t.getName());
        if (exist != null) {
            throw new BasketException("Team already exists.");
        }
        em.persist(t);
        em.close();
    }

    public List<Team> selectAllTeams() {
        return emf.createEntityManager().createNamedQuery("Team.findAll").getResultList();
    }

    public List<String> selectAllCityTeams() {
        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("select distinct t.city from Team t");
        return q.getResultList();
    }

    public List<Team> selectTeamsByCity(String city) {
        EntityManager em = emf.createEntityManager();
        Query q = em.createNamedQuery("Team.findByCity");
        q.setParameter("city", city);
        return q.getResultList();
    }

}
