/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mfontana
 */
@Entity
@Table(name = "player")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Player.findAll", query = "SELECT p FROM Player p"),
    @NamedQuery(name = "Player.findByName", query = "SELECT p FROM Player p WHERE p.name = :name"),
    @NamedQuery(name = "Player.findByBirth", query = "SELECT p FROM Player p WHERE p.birth = :birth"),
    @NamedQuery(name = "Player.findByNbaskets", query = "SELECT p FROM Player p WHERE p.nbaskets = :nbaskets"),
    @NamedQuery(name = "Player.findByNassists", query = "SELECT p FROM Player p WHERE p.nassists = :nassists"),
    @NamedQuery(name = "Player.findByNrebounds", query = "SELECT p FROM Player p WHERE p.nrebounds = :nrebounds"),
    @NamedQuery(name = "Player.findByPosition", query = "SELECT p FROM Player p WHERE p.position = :position")})
public class Player implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "birth")
    @Temporal(TemporalType.DATE)
    private Date birth;
    @Column(name = "nbaskets")
    private Integer nbaskets;
    @Column(name = "nassists")
    private Integer nassists;
    @Column(name = "nrebounds")
    private Integer nrebounds;
    @Size(max = 100)
    @Column(name = "position")
    private String position;
    @JoinColumn(name = "team", referencedColumnName = "name")
    @ManyToOne
    private Team team;

    public Player() {
    }

    public Player(String name) {
        this.name = name;
    }

    public Player(String name, Date birth) {
        this.name = name;
        this.birth = birth;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public Integer getNbaskets() {
        return nbaskets;
    }

    public void setNbaskets(Integer nbaskets) {
        this.nbaskets = nbaskets;
    }

    public Integer getNassists() {
        return nassists;
    }

    public void setNassists(Integer nassists) {
        this.nassists = nassists;
    }

    public Integer getNrebounds() {
        return nrebounds;
    }

    public void setNrebounds(Integer nrebounds) {
        this.nrebounds = nrebounds;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (name != null ? name.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Player)) {
            return false;
        }
        Player other = (Player) object;
        if ((this.name == null && other.name != null) || (this.name != null && !this.name.equals(other.name))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Player[ name=" + name + " ]";
    }
    
}
